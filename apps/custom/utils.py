import json
import re
import smtplib
from datetime import datetime
from email._parseaddr import COMMASPACE
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from os.path import basename

from django.core.serializers.json import DjangoJSONEncoder
from django.http.response import HttpResponse
from django.utils.encoding import force_unicode

from crm.settings import EMAIL_HOST, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD
from ..orders.constants import DATE_FORMAT


def get_json_response(python_dict):
    json_string = json.dumps(python_dict, cls=DjangoJSONEncoder)
    return HttpResponse(json_string, content_type='application/json')


class newdict(dict):
    def __setitem__(self, name, value):
        if value is None:
            setattr(self, name, '')
        # elif type(value) is datetime:
        #     setattr(self, name, value.strftime('%Y-%m-%d'))
        else:
            super(newdict, self).__setitem__(name, value)


class Echo(object):
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


def newgetattr(instance, item):
    value = getattr(instance, item)
    if type(value) is datetime:
        return value.strftime(DATE_FORMAT)
    elif type(value) is unicode:
        return value.encode('utf-8')
    else:
        return value


def to_unicode(bytestr):
    """
    Receive string bytestr and try to return a utf-8 string.
    """
    if not isinstance(bytestr, str) and not isinstance(bytestr, unicode):
        return bytestr

    try:
        decoded = bytestr.decode("cp1252")  # default by excel csv
    except UnicodeEncodeError:
        decoded = force_unicode(bytestr, "utf-8")
    return decoded


def send_mail_attachment(send_to, subject, text, files=None):
    assert isinstance(send_to, list)

    msg = MIMEMultipart(
        From=EMAIL_HOST_USER,
        To=COMMASPACE.join(send_to),
        Date=formatdate(localtime=True),
        Subject=subject
    )
    msg.add_header('Subject', subject)
    msg.attach(MIMEText(text))

    for f in files or []:
        with open(f, "rb") as fil:
            msg.attach(MIMEApplication(
                fil.read(),
                Content_Disposition='attachment; filename="%s"' % basename(f),
                Name=basename(f)
            ))

    server = smtplib.SMTP(EMAIL_HOST)
    server.starttls()
    server.login(EMAIL_HOST_USER, EMAIL_HOST_PASSWORD)
    server.sendmail(EMAIL_HOST, send_to, msg.as_string())
    server.close()


def extract_int(string, encode=True):
    if encode:
        return ''.join(re.findall(r'\d+', string.encode('utf-8')))
    else:
        return ''.join(re.findall(r'\d+', string))
