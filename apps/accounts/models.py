from __future__ import unicode_literals
from django.contrib.auth.models import User

from django.db import models


class Account(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    ROLE = (('CX', 'CX'),
            ('SRM', 'SRM'),
            ('Admin', 'Admin'))
    role = models.CharField(choices=ROLE, max_length=10, null=True, blank=True)

    def __unicode__(self):
        return '{}'.format(self.user.username)

