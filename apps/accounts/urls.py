from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('apps.accounts.views',
                       url(r'^$', LoginView.as_view()),
                       url(r'^logout/$', logout_user),
                       url(r'^change_password/$', change_password)
                       )
