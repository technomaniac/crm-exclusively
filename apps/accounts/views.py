from django.contrib.auth import authenticate, login, logout
from django.http.response import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.views.generic.base import View
from crm.settings import LOGIN_URL


class LoginView(View):
    template_name = 'registration/login.html'

    def post(self, request):
        user = authenticate(username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            # the password verified for the user
            if user.is_active:
                login(request, user)
                url = request.GET.get('next', '/orders/dashboard/')
                return HttpResponseRedirect(url)
        else:
            # the authentication system was unable to verify the username and password
            return render(request, self.template_name, {'errors': 'Wrong username/password'})

    def get(self, request):
        if request.user.is_authenticated():
            url = request.GET.get('next', '/orders/dashboard/')
            return HttpResponseRedirect(url)
        return render(request, self.template_name)


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(LOGIN_URL)


def change_password(request):
    if request.method == 'POST':
        old_pass = request.POST['old_password']
        user = authenticate(username=request.user.username, password=old_pass)
        if user is not None:

            new_password = request.POST['new_password']
            confirm_password = request.POST['confirm_password']
            if new_password == confirm_password:
                user.set_password(new_password)
                user.save()
                login(request, user)
                url = request.GET.get('next', '/orders/dashboard/')
                return HttpResponseRedirect(url)
            else:
                return render(request, 'registration/reset_password.html', {'errors': 'Password do not match'})
        else:
            return render(request, 'registration/reset_password.html', {'errors': 'Old password is incorrect'})
    else:
        return render(request, 'registration/reset_password.html')
