import itertools

import django_tables2 as tables

from .constants import DATE_FORMAT
from .models import Orders


class OrderTable(tables.Table):
    s_no = tables.Column(empty_values=(), orderable=False, verbose_name='S.No.')
    _50_per_calling_remark = tables.Column(verbose_name='50% Calling Remark', empty_values=(),
                                           accessor='_50_per_calling_remark_',
                                           order_by='order_editable___50_per_calling_remark')
    _50_per_calling_edd = tables.Column(verbose_name='50% Calling EDD', empty_values=(),
                                        accessor='order_editable._50_per_calling_edd',
                                        order_by='order_editable___50_per_calling_edd')
    _2_days_calling_remark = tables.Column(verbose_name='2 Days Calling Remark', empty_values=(),
                                           accessor='_2_days_calling_remark_',
                                           order_by='order_editable___2_days_calling_remark')
    _2_days_calling_edd = tables.Column(verbose_name='2 Days Calling EDD', empty_values=(),
                                        accessor='order_editable._2_days_calling_edd',
                                        order_by='order_editable___2_days_calling_edd')
    overall_status = tables.Column(verbose_name='Overall Status', empty_values=(),
                                   accessor='_overall_status_',
                                   order_by='order_editable__overall_status')
    edd = tables.Column(verbose_name='EDD', empty_values=(), accessor='order_editable.edd',
                        order_by='order_editable__edd')
    _50_per_mail_sent = tables.Column(verbose_name='50% Mail Sent',
                                      accessor='order_editable._50_per_mail_sent',
                                      order_by='order_editable___50_per_mail_sent')
    _2_days_mail_sent = tables.Column(verbose_name='2 Days Mail Sent',
                                      accessor='order_editable._2_days_mail_sent',
                                      order_by='order_editable___2_days_mail_sent')
    # scheduled_pickup = tables.Column(verbose_name='Scheduled Pickup', empty_values=(), accessor='_scheduled_pickup_',
    #                                  order_by='order_editable__scheduled_pickup')
    # scheduled_pickup_date = tables.Column(verbose_name='Scheduled Pickup Date', empty_values=(),
    #                                       accessor='_scheduled_pickup_date_',
    #                                       order_by='order_editable__scheduled_pickup_date')
    pickup_request_time = tables.Column(verbose_name='Pickup Request Time', empty_values=(),
                                        accessor='_pickup_request_time_',
                                        order_by='pickup_request_time')

    remarks = tables.Column(verbose_name='Remarks', empty_values=(),
                            accessor='order_editable.remarks',
                            order_by='order_editable__remarks')
    action = tables.TemplateColumn(template_code='<button type="submit" id="{{ record.id }}" >Edit</button>')

    class Meta:
        model = Orders
        attrs = {"class": "table table-bordered table-condensed table-striped table-hover",
                 "data-count-fixed-columns": "2", "cellpadding": "0", "cellspacing": "0"}
        exclude = ['id', 'created_on', 'updated_on', 'is_active', 'order_id', 'awb', 'shipping_time', 'courier_partner',
                   'shipping_state', 'shipping_city', 'sku_type', 'shipping_edd', 'num_days_delay', 'vendor_code']
        sequence = (
            's_no', 'order_time', 'increment_id', 'sku', 'vendor_sku', 'product_name', 'vendor', 'vrm',
            'po_no', 'po_creation_time', 'po_acceptance_time', 'po_status', 'customer_edd', 'vendor_edd',
            'edd_as_per_vendor')
        per_page = 10

    def __init__(self, *args, **kwargs):
        per_page = int(kwargs.pop('per_page', 15))
        page = int(kwargs.pop('page', 1))
        offset = per_page * (page - 1)
        role = kwargs.pop('role')
        super(OrderTable, self).__init__(*args, **kwargs)
        self.counter = itertools.count(start=offset)
        if role == 'CX':
            self.exclude = ('delayed_status_vendor', 'vendor_sku', 'customer_sla', 'vendor_sla',
                            'po_no', 'po_acceptance_time', 'po_status', 'po_creation_time', 'grn_number',
                            'qc_status', 'reason', 'qc_comment', 'qc_time', 'rtv_no', 'rtv_time', 'category',
                            'sub_category', 'vendor_edd', 'edd_as_per_vendor', 'qty_ordered', 'qty_refunded',
                            'qty_shipped', '_50_per_vendor_edd', '_2_days_vendor_edd', 'pickup_request_time',
                            'remarks')

            self.sequence = (
                's_no', 'order_time', 'increment_id', 'sku', 'vendor_sku', 'product_name', 'vendor', 'vrm', 'po_no',
                'po_creation_time', 'po_acceptance_time', 'po_status', 'customer_edd', 'vendor_edd',
                'revised_vendor_edd', 'revised_customer_edd', 'gsv', 'customer_name', 'customer_email',
                'customer_contact', 'delayed_status_customer', 'shipping_country', '_50_per_calling_remark',
                '_50_per_calling_edd', '_2_days_calling_remark', '_2_days_calling_edd', 'overall_status', 'edd',
                '_50_per_customer_edd', '_2_days_customer_edd', '_50_per_mail_sent', '_2_days_mail_sent')

        if role == 'SRM':
            self.exclude = (
                'category', 'sub_category', 'gsv', 'customer_name', 'customer_email', 'customer_contact',
                '_50_per_mail_sent', '_2_days_mail_sent', 'revised_customer_edd', 'qty_shipped', 'qty_refunded')

    def render_s_no(self):
        return next(self.counter) + 1

    def render_remarks(self, **kwargs):
        if kwargs['record'].order_editable.remarks is None:
            return ''
        return kwargs['record'].order_editable.remarks

    def render_order_time(self, **kwargs):
        if kwargs['record'].order_time:
            return kwargs['record'].order_time.strftime(DATE_FORMAT)
        return ''

    def render_scheduled_pickup_date(self, **kwargs):
        if kwargs['record']._scheduled_pickup_date_:
            return kwargs['record']._scheduled_pickup_date_.strftime(DATE_FORMAT)
        return ''

    def render_customer_edd(self, **kwargs):
        if kwargs['record'].customer_edd:
            return kwargs['record'].customer_edd.strftime(DATE_FORMAT)
        return ''

    def render__50_per_customer_edd(self, **kwargs):
        if kwargs['record']._50_per_customer_edd:
            return kwargs['record']._50_per_customer_edd.strftime(DATE_FORMAT)
        return ''

    def render__2_days_customer_edd(self, **kwargs):
        if kwargs['record']._2_days_customer_edd:
            return kwargs['record']._2_days_customer_edd.strftime(DATE_FORMAT)
        return ''

    def render_vendor_edd(self, **kwargs):
        if kwargs['record'].vendor_edd:
            return kwargs['record'].vendor_edd.strftime(DATE_FORMAT)
        return ''

    def render_shipping_edd(self, **kwargs):
        if kwargs['record'].shipping_edd:
            return kwargs['record'].shipping_edd.strftime(DATE_FORMAT)
        return ''

    def render_revised_customer_edd(self, **kwargs):
        if kwargs['record'].revised_customer_edd:
            return kwargs['record'].revised_customer_edd.strftime(DATE_FORMAT)
        return ''

    def render_revised_vendor_edd(self, **kwargs):
        if kwargs['record'].revised_vendor_edd:
            return kwargs['record'].revised_vendor_edd.strftime(DATE_FORMAT)
        return ''

    def render_edd_as_per_vendor(self, **kwargs):
        if kwargs['record'].edd_as_per_vendor:
            return kwargs['record'].edd_as_per_vendor.strftime(DATE_FORMAT)
        return ''

    def render_po_creation_time(self, **kwargs):
        if kwargs['record'].po_creation_time:
            return kwargs['record'].po_creation_time.strftime(DATE_FORMAT)
        return ''

    def render_qc_time(self, **kwargs):
        if kwargs['record'].qc_time:
            return kwargs['record'].qc_time.strftime(DATE_FORMAT)
        return ''

    def render_po_acceptance_time(self, **kwargs):
        if kwargs['record'].po_acceptance_time:
            return kwargs['record'].po_acceptance_time.strftime(DATE_FORMAT)
        return ''

    def render_rtv_time(self, **kwargs):
        if kwargs['record'].rtv_time:
            return kwargs['record'].rtv_time.strftime(DATE_FORMAT)
        return ''

    def render_shipping_time(self, **kwargs):
        if kwargs['record'].shipping_time:
            return kwargs['record'].shipping_time.strftime(DATE_FORMAT)
        return ''

    def render__50_per_vendor_edd(self, **kwargs):
        if kwargs['record']._50_per_vendor_edd:
            return kwargs['record']._50_per_vendor_edd.strftime(DATE_FORMAT)
        return ''

    def render__2_days_vendor_edd(self, **kwargs):
        if kwargs['record']._2_days_vendor_edd:
            return kwargs['record']._2_days_vendor_edd.strftime(DATE_FORMAT)
        return ''

    def render__50_per_calling_edd(self, **kwargs):
        if kwargs['record'].order_editable._50_per_calling_edd:
            return kwargs['record'].order_editable._50_per_calling_edd.strftime(DATE_FORMAT)
        return ''

    def render__2_days_calling_edd(self, **kwargs):
        if kwargs['record'].order_editable._2_days_calling_edd:
            return kwargs['record'].order_editable._2_days_calling_edd.strftime(DATE_FORMAT)
        return ''

    def render_inward_time(self, **kwargs):
        if kwargs['record'].inward_time:
            return kwargs['record'].inward_time.strftime(DATE_FORMAT)
        return ''

    def render_edd(self, **kwargs):
        if kwargs['record'].order_editable.edd:
            return kwargs['record'].order_editable.edd.strftime(DATE_FORMAT)
        return ''

        # def render__50_per_calling_remark(self, **kwargs):
        #     if kwargs['record']._50_per_calling_remark_:
        #         return kwargs['record']._50_per_calling_remark_
        #     return ''
