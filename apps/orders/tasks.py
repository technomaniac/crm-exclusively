import json
import os
import re
import subprocess
import zipfile
from datetime import datetime, timedelta
from ftplib import FTP
from os import environ

import mysql
import requests
import xlrd
from celery.task import task
from django.conf import settings
from django.core.cache import cache
from django.core.mail.message import EmailMultiAlternatives
from django.db.models import Q
from django.template.context import Context
from django.template.loader import get_template

from crm.settings import get_mysql_connection, OMNITURE_FTP_HOST, OMNITURE_FTP_USER, OMNITURE_FTP_PASS, EMAIL_HOST_USER
from .constants import OMNITURE_ANDROID_REPORT_MAPPING
from .constants import _50_per_calling_remark_choice, _2_days_calling_remark_choice, overall_status_choice, \
    omniture_report_mailing_list
from .models import Orders, OrderEditable, CRM
from .utils import filter_ticket, AWRMAEntity
from ..custom.utils import extract_int


@task
def populate_orders():
    print "populating orders"
    cnx = get_mysql_connection()
    cursor = cnx.cursor()
    sql = '''SELECT * FROM analytics.ng_pending_dump_2;'''
    cursor.execute(sql)
    rows = cursor.fetchall()
    cnx.close()
    cnx.disconnect()
    order_list = list()
    i = 0
    for row in rows:
        data = CRM(*row).__dict__
        increment_id = data.pop('increment_id')
        sku = data.pop('sku')
        data['is_active'] = True
        order, created = Orders.objects.update_or_create(sku=sku, increment_id=increment_id, defaults=data)

        if created:
            i += 1
            OrderEditable.objects.create(order=order)
            insert_sql = "insert into analytics.ng_pending_dump_2_status (increment_id,sku)"
            insert_sql += " values ('{}','{}');".format(increment_id, sku)
            cnx = get_mysql_connection()
            cursor = cnx.cursor()
            cursor.execute(insert_sql)
            cnx.commit()
            cnx.close()
            cnx.disconnect()
        order_list.append(order)

    print '{} new orders'.format(i)
    not_updated_orders = Orders.objects.filter(~Q(pk__in=[i.pk for i in order_list]))
    not_updated_orders.update(is_active=False)
    if len(order_list) > 0:
        delete_sql = "DELETE FROM analytics.ng_pending_dump_2_status WHERE (increment_id,sku)"
        delete_sql += " NOT IN ({})".format(
            ",".join(["('{}','{}')".format(i.increment_id, i.sku) for i in order_list]))
        cnx = get_mysql_connection()
        cursor = cnx.cursor()
        cursor.execute(delete_sql)
        cnx.commit()
        cnx.close()
        cnx.disconnect()
        print '{} orders deleted'.format(len(not_updated_orders))
    OrderEditable.objects.filter(Q(order__in=order_list)).update(is_active=True)
    OrderEditable.objects.filter(~Q(order__in=order_list)).update(is_active=False)
    print 'Total {} Orders found'.format(len(order_list))
    # insert = [Orders(**) for row in rows]


@task
def update_analytics_db(data):
    cnx = get_mysql_connection()
    cursor = cnx.cursor()
    sql = "select * from analytics.ng_pending_dump_2_status where increment_id='{}' and sku='{}'".format(
        data.get('increment_id'), data.get('sku'))
    cursor.execute(sql)
    rows = cursor.fetchall()
    if len(rows) == 0:
        insert_sql = "insert into analytics.ng_pending_dump_2_status (sku,increment_id)"
        insert_sql += " values ('{}','{}');".format(data.get('sku'), data.get('increment_id'))
        cursor.execute(insert_sql)
        cnx.commit()
    cnx.close()
    cnx.disconnect()

    if data.get('_50_per_calling_remark'):
        data['_50_per_calling_remark'] = dict(_50_per_calling_remark_choice)[data['_50_per_calling_remark']]
    if data.get('_2_days_calling_remark'):
        data['_2_days_calling_remark'] = dict(_2_days_calling_remark_choice)[data['_2_days_calling_remark']]
    if data.get('overall_status'):
        data['overall_status'] = dict(overall_status_choice)[data['overall_status']]

    cnx = get_mysql_connection()
    cursor = cnx.cursor()
    where_clause = "where sku='{}' and increment_id='{}'".format(data.pop('sku'), data.pop('increment_id'))
    set_statement = "set " + ", ".join(["{}='{}'".format(key, value) for key, value in data.iteritems()])
    sql = '''update analytics.ng_pending_dump_2_status {} {}'''.format(set_statement, where_clause)
    cursor.execute(sql)
    cnx.commit()
    cnx.close()
    cnx.disconnect()


@task
def zendesk_populate_tickets():
    cnx = get_mysql_connection()
    cursor = cnx.cursor()
    cursor.execute("""truncate zendesk_tickets""")
    cnx.commit()
    cnx.close()
    cnx.disconnect()

    user = environ.get('ZENDESK_USER', 'ankit@exclusively.in')
    password = environ.get('ZENDESK_PASS', 'singh1947')

    url = 'https://exclusivelyin.zendesk.com/api/v2/tickets.json'
    while True:
        r = requests.get(url, auth=(user, password))
        data = r.json()
        ticket = dict()

        for i in data['tickets']:
            if filter_ticket(i['created_at']):
                sql = """insert into zendesk_tickets (requester_email, status, via, created_at, description,order_no, tags, ticket_type, requester_name, priority,updated_at, assignee_id, satisfaction_score, tags_for_agents,requester_id, group_id, id, subject) values """

                ticket['id'] = i['id']
                ticket['requester_id'] = i['requester_id']
                ticket['via'] = i['via']['channel']
                if ticket['via'] == 'email':
                    try:
                        ticket['requester_name'] = re.escape(i['via']['source']['from']['name'].encode('utf-8').strip())
                    except:
                        ticket['requester_name'] = i['via']['source']['from']['name'].encode('utf-8').strip()

                    try:
                        ticket['requester_email'] = re.escape(
                            i['via']['source']['from']['address'].encode('utf-8').strip())
                    except:
                        ticket['requester_email'] = i['via']['source']['from']['address'].encode('utf-8').strip()
                else:
                    ticket['requester_name'] = ''
                    ticket['requester_email'] = ''
                ticket['assignee_id'] = i['assignee_id']
                ticket['group_id'] = i['group_id']
                try:
                    ticket['subject'] = re.escape(i['subject'].encode('utf-8').strip())
                except:
                    ticket['subject'] = i['subject'].encode('utf-8').strip()

                try:
                    ticket['description'] = re.escape(i['description'].encode('utf-8').strip())
                except:
                    ticket['description'] = i['description'].encode('utf-8').strip()
                ticket['tags'] = '|'.join(i['tags'])
                ticket['status'] = i['status']
                ticket['priority'] = i['priority']
                ticket['ticket_type'] = i['type']
                ticket['created_at'] = i['created_at']
                ticket['updated_at'] = i['updated_at']
                for item in i['custom_fields']:
                    if item['id'] == 22920940:
                        ticket['tags_for_agents'] = item['value']

                    if item['id'] == 22755764:
                        ticket['order_no'] = item['value']
                ticket['satisfaction_score'] = i['satisfaction_rating']['score']
                try:
                    values = """('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}')""".format(
                        ticket['requester_email'],
                        ticket['status'],
                        ticket['via'],
                        ticket['created_at'],
                        ticket['description'],
                        ticket['order_no'],
                        ticket['tags'],
                        ticket['ticket_type'],
                        ticket['requester_name'],
                        ticket['priority'],
                        ticket['updated_at'],
                        ticket['assignee_id'],
                        ticket['satisfaction_score'],
                        ticket['tags_for_agents'],
                        ticket['requester_id'],
                        ticket['group_id'],
                        ticket['id'],
                        ticket['subject'])

                    sql += """{}""".format(values)
                    cnx = get_mysql_connection()
                    cursor = cnx.cursor()
                    cursor.execute(sql)
                    cnx.commit()
                    cnx.close()
                    cnx.disconnect()
                except Exception as e:
                    print e

        if data['next_page']:
            url = data['next_page']
        else:
            break

    url = 'https://exclusivelyin.zendesk.com/api/v2/ticket_metrics.json'
    while True:
        r = requests.get(url, auth=(user, password))
        data = r.json()

        for i in data['ticket_metrics']:
            if filter_ticket(i['created_at']):
                sql = "update zendesk_tickets set "
                sql += "assigned_at='{}',".format(i['assigned_at'])
                sql += "initially_assigned_at='{}',".format(i['initially_assigned_at'])
                sql += "solved_at='{}',".format(i['solved_at'])
                sql += "reopens='{}',".format(i['reopens'])
                sql += "replies='{}',".format(i['replies'])
                sql += "first_resolution_time_in_minutes='{}',".format(
                    i['first_resolution_time_in_minutes']['calendar'])
                sql += "full_resolution_time_in_minutes='{}',".format(i['full_resolution_time_in_minutes']['calendar'])
                sql += "agent_wait_time_in_minutes='{}',".format(i['agent_wait_time_in_minutes']['calendar'])
                sql += "requester_wait_time_in_minutes='{}',".format(i['requester_wait_time_in_minutes']['calendar'])
                sql += "on_hold_time_in_minutes='{}',".format(i['on_hold_time_in_minutes']['calendar'])
                sql += "reply_time_in_minutes='{}',".format(i['reply_time_in_minutes']['calendar'])
                sql += "latest_comment_added_at='{}'".format(i['latest_comment_added_at'])
                sql += " where id='{}'".format(i['ticket_id'])

                try:
                    cnx = get_mysql_connection()
                    cursor = cnx.cursor()
                    cursor.execute(sql)
                    cnx.commit()
                    cnx.close()
                    cnx.disconnect()
                except Exception as e:
                    print e

        if data['next_page']:
            url = data['next_page']
        else:
            break


@task
def serialize_exin_community():
    import mysql.connector
    mysql_config = {'user': 'Cr0nqueue',
                    'password': 'R0ng5u$yh7u',
                    'host': os.environ.get('MYSQL_HOST', '52.76.57.253'),
                    'database': 'exin_community',
                    'buffered': True}

    cnx = mysql.connector.connect(**mysql_config)
    cursor = cnx.cursor()
    cursor.execute("""truncate exin_community.aw_rma_entity_serialized""")
    cnx.commit()
    cursor = cnx.cursor()
    sql = '''select * from exin_community.aw_rma_entity;'''
    cursor.execute(sql)
    rows = cursor.fetchall()

    if len(rows) > 0:
        insert_sql = '''insert into exin_community.aw_rma_entity_serialized (order_id,order_items) values {}'''
        values = ""
        for row in rows:
            item = AWRMAEntity(*row)
            if item.order_items is None:
                values += "('{}','{}'),".format(item.order_id, item.order_items)
            else:
                for i in item.order_items:
                    values += "('{}',{}),".format(item.order_id, i)

        cursor.execute(insert_sql.format(values[:-1]))
        cnx.commit()
        cnx.close()
        cnx.disconnect()


@task
def fetch_user_details():
    timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    cnx = get_mysql_connection()
    cursor = cnx.cursor()

    select_sql = "select id from user_all_account_details where email = '{}'"
    insert_sql = "insert into user_all_account_details (email,first_name,last_name,sex,created,updated) values ({})"
    update_sql = "update user_all_account_details set first_name='{}',last_name='{}',sex='{}',created='{}',updated='{}' where email='{email}'"

    base_url = 'http://api.exclusively.com/uaa/getAllAccountDetails'
    headers = {'USER_FETCH_TOKEN': 'quintessential'}

    page = 1
    size = 100
    while True:

        url = base_url + '?pageNumber={}&pageSize={}'.format(page, size)
        r = requests.get(url, headers=headers)
        data = json.loads(r.content)
        if len(data) > 0:
            for i in data:

                row = []
                for k in i:
                    if k == None:
                        row.append('')
                    else:
                        row.append(k)
                row.append(timestamp)

                cursor.execute(select_sql.format(i[0]))
                if len(cursor.fetchall()) == 0:
                    values = ""
                    for r in row:
                        values += "'{}',".format(r)

                    cursor.execute(insert_sql.format(values[:-1]))
                    cnx.commit()

                else:
                    cursor.execute(update_sql.format(*row[1:], email=row[0]))
                    cnx.commit()

            page += 1
        else:
            break


@task
def send_omniture_report():
    ftp = FTP(OMNITURE_FTP_HOST)
    ftp.login(OMNITURE_FTP_USER, OMNITURE_FTP_PASS)
    ftp.cwd('/Exclusively_Omniture/App android daily conversions')
    data = []
    ftp.dir('-t', data.append)

    line = data[0].split()
    filename = ' '.join(line[8:])

    android_report_time = ' '.join([line[7], line[6], line[5], datetime.now().strftime('%Y')])
    android_report_time = datetime.strptime(android_report_time, '%H:%M %d %b %Y')

    if cache.get('last_android_report_time') is None or \
            (cache.get('last_android_report_time') and android_report_time > cache.get('last_android_report_time')):

        android_report_time = android_report_time + timedelta(hours=5, minutes=30)
        android_report_time = android_report_time.strftime('%H:%M %d %b, %Y')

        new_file = '{}/omniture/Android_Daily_Conversions_{}.xlsx'.format(settings.MEDIA_ROOT,
                                                                          datetime.now().strftime('%d-%b-%Y-%H-%M'))
        ftp.retrbinary('RETR %s' % filename, open(new_file, 'wb').write)
        ftp.quit()

        workbook = xlrd.open_workbook(new_file)
        sheet = workbook.sheet_by_index(0)

        context = []

        for row in xrange(19, sheet.nrows):
            col = [sheet.cell(row, col).value for col in xrange(1, sheet.ncols)]
            data = dict()

            try:
                data['page'] = OMNITURE_ANDROID_REPORT_MAPPING[col[0]]
                if col[0] == 'HomeLauncherActivity':
                    continue


                elif col[0] in ['wap:orderComplete', 'app:orderComplete']:
                    order_complete = [context.index(i) for i in context if i['page'] == 'Order Complete']
                    if order_complete:
                        context[order_complete[0]]['visits'] += int(col[1])
                        context[order_complete[0]]['perc'] += float(col[2] * 100)
                        context[order_complete[0]]['perc'] = "{0:.2f}".format(context[order_complete[0]]['perc'])
                    else:
                        data['visits'] = int(col[1])
                        data['perc'] = float(col[2] * 100)
                        context.append(data)
                    continue

                elif col[0] in ['wap:ShippingDetail ', 'App:ShippingDetail ']:
                    order_complete = [context.index(i) for i in context if i['page'] == 'Visits to Shipping Page']
                    if order_complete:
                        context[order_complete[0]]['visits'] += int(col[1])
                        context[order_complete[0]]['perc'] += float(col[2] * 100)
                        context[order_complete[0]]['perc'] = "{0:.2f}".format(context[order_complete[0]]['perc'])
                    else:
                        data['visits'] = int(col[1])
                        data['perc'] = float(col[2] * 100)
                        context.append(data)
                    continue

                elif col[0] in ['App:PaymentDetail', 'wap:PaymentDetail']:
                    order_complete = [context.index(i) for i in context if i['page'] == 'Visits to Payment Page']
                    if order_complete:
                        context[order_complete[0]]['visits'] += int(col[1])
                        context[order_complete[0]]['perc'] += float(col[2] * 100)
                        context[order_complete[0]]['perc'] = "{0:.2f}".format(context[order_complete[0]]['perc'])
                    else:
                        data['visits'] = int(col[1])
                        data['perc'] = float(col[2] * 100)
                        context.append(data)
                    continue

                elif col[0] == 'Total':
                    data['visits'] = int(col[1])
                    data['perc'] = "{0:.2f}".format(float(100))
                    context = [data] + context
                    continue
                else:
                    data['visits'] = int(col[1])
                    data['perc'] = "{0:.2f}".format(float(col[2] * 100))
                    context.append(data)

            except:
                data['page'] = col[0]
                data['visits'] = int(col[1])
                data['perc'] = "{0:.2f}".format(float(col[2] * 100))
                context.append(data)

        html = get_template('orders/omniture_app_report.html')

        d = Context({'android_report': context, 'android_report_time': android_report_time,
                     'datetime': datetime.now().strftime('%I %p %b %d, %Y')})

        subject = 'Omniture App Conversion Report'

        html_content = html.render(d)
        msg = EmailMultiAlternatives(subject, 'Goal Conversion Ratio', from_email=EMAIL_HOST_USER,
                                     to=omniture_report_mailing_list)
        msg.attach_alternative(html_content, "text/html")
        msg.send()
        print 'Report Sent.'

        os.remove(new_file)
    else:
        print 'Report Not Sent.'

    cache.set('last_android_report_time', android_report_time)


@task
def app_score_data():
    try:
        ftp = FTP(OMNITURE_FTP_HOST)
        ftp.login(OMNITURE_FTP_USER, OMNITURE_FTP_PASS)
        ftp.cwd('/Exclusively_Omniture/App DWH daily/')
        data = []
        ftp.dir('-t', data.append)

        for l in data:
            line = l.split()
            filename = ' '.join(line[8:])

            app_score_time = datetime.strptime(' '.join([line[7], line[6], line[5], datetime.now().strftime('%Y')]),
                                               '%H:%M %d %b %Y') + timedelta(hours=5, minutes=30)

            cache.delete('last_app_score_time')
            if cache.get('last_app_score_time') is None or \
                    (cache.get('last_app_score_time') and app_score_time > cache.get('last_app_score_time')):

                zip_file = '{}/omniture/{}'.format(settings.MEDIA_ROOT, filename)
                ftp.retrbinary('RETR %s' % filename, open(zip_file, 'wb').write)
                #ftp.quit()

                with zipfile.ZipFile(zip_file, "r") as z:
                    z.extractall('{}/omniture/'.format(settings.MEDIA_ROOT))
                    csv = z.namelist()[0]
                    csv_file = '{}/omniture/{}'.format(settings.MEDIA_ROOT, csv)

                    r_file = os.path.join(os.path.dirname(__file__), 'app_score.R')

                    proc = subprocess.Popen(['Rscript', r_file, csv_file],
                                            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    stdout, stderr = proc.communicate()
                    date = datetime.strptime(extract_int(csv), '%Y%m%d').strftime('%Y-%m-%d')
                    print stdout, stderr
                    data = stdout.split()
                    print date
                    cnx = get_mysql_connection()
                    cursor = cnx.cursor()

                    sql = """insert into analytics.app_score_card (report_date,os,facebook,google,freecharge,identified_login,unidentified_login,not_logged,total_session) values {}"""
                    score_data = [[date] + data[7:13] + data[23:25], [date] + data[14:20] + data[26:28]]

                    # print score_data
                    value_list = []
                    # print score_data
                    for score in score_data:
                        v = "('{}','{}',".format(score[0], score[1])
                        v += "{})".format(','.join(score[2:]))
                        value_list.append(v)

                    values = "{}".format(",".join(value_list))
                    sql = sql.format(values)

                    cursor.execute(sql)
                    cnx.commit()
                    cnx.close()
                    cnx.disconnect()

                    os.remove(csv_file)
                os.remove(zip_file)
            else:
                print 'Not Created'

            cache.set('last_app_score_time', app_score_time)

        ftp.quit()
    except Exception as e:
        print e.message
