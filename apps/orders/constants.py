__author__ = 'abhishek'

DATE_FORMAT = '%d/%m/%Y'

_50_per_calling_remark_choice = (
    ('IP-DPS', 'In production - will be delivered as per SLA'),
    ('IP-ADF', 'In production - need additional days to fulfillment'),
    ('SNR', 'Seller not responding'),
    ('NIP-DPS', 'Not in production - will deliver as per SLA'),
    ('NIP-ADOF', 'Not in production - will need additional days for order fulfillment'),
    ('NIP-OC', 'Not in production - order cancellation'),
    ('NIP-DPI', 'Not in production - Due to Payment Issue'),
    ('PI-DID', 'Payment Issue - Delay in Dispatch')
)

_2_days_calling_remark_choice = (
    ('DPS', 'Dispatched as per SLA'),
    ('PPS', 'Pickup as per SLA'),
    ('DEL', 'Delayed')
)

overall_status_choice = (
    ('EDDA', 'EDD Available'),
    ('DD', 'Dispatched by Designer'),
    ('QCRFU', 'QCR Follow up'),
    ('PI', 'Payment issue'),
    ('DR', 'No Update from Designer'),
    ('CAN', 'To be Cancelled'),
    ('TI', 'Tech Issue'),
    ('PC', 'Pickup Confirmed')
)

mail_sent_choice = (('Y', 'Yes'), ('N', 'No'))

OMNITURE_ANDROID_REPORT_MAPPING = {'HomeLauncherActivity': 'No of App Launches',
                                   'CategoryDisplayPageActivity': 'Visits to Listing Page',
                                   'PDPScreenView': 'Visits to PDP Page',
                                   'ShoppingBagMainActivity': 'Visits to Cart Page',
                                   'wap:orderComplete': 'Order Complete',
                                   'app:orderComplete': 'Order Complete',
                                   'Total': 'Total Visits',
                                   'wap:ShippingDetail': 'Visits to Shipping Page',
                                   'App:ShippingDetail': 'Visits to Shipping Page',
                                   'App:PaymentDetail': 'Visits to Payment Page',
                                   'wap:PaymentDetail': 'Visits to Payment Page'}

omniture_report_mailing_list = ['abhishek.verma03@exclusively.com',
                                'satish.korrapati@snapdeal.com',
                                'manash.das@exclusively.com',
                                'brajesh.rawat@exclusively.com',
                                'chauhan.vivek@exclusively.com',
                                'raj@exclusively.com', 'vivek.singh01@exclusively.com',
                                'animesh@exclusively.com',
                                'rohan.ranjan@exclusively.com', 'jayant.yadav@exclusively.com',
                                'ankur.panesar@exclusively.com', 'jayasim.nsr@exclusively.com',
                                'badrinath.mishra@exclusively.com', 'nipun.agarwal@exclusively.com',
                                'analytics@exclusively.com']
