from datetime import datetime
import re

from django.db.models.query_utils import Q

from ..custom.utils import newgetattr
from .constants import DATE_FORMAT
from .models import Orders


def get_date_range(date_string):
    dates = date_string.split('-')
    start_date = datetime.strptime(dates[0] + ' 00:00:00', DATE_FORMAT + ' %H:%M:%S')
    end_date = datetime.strptime(dates[1] + ' 23:59:59', DATE_FORMAT + ' %H:%M:%S')
    return (start_date, end_date)


def extract_numbers(string, encode=True):
    if encode:
        return ''.join(re.findall(r'\d+', string.encode('utf-8')))
    else:
        return ''.join(re.findall(r'\d+', string))

def get_dashboard_context(filter):
    context = dict()
    context['datetime'] = datetime.now().strftime('%H:%M %b, %d %Y')
    vrm = Orders.objects.filter(is_active=True).values('vrm').distinct()

    if filter.get('vendor__icontains'):
        filter.pop('vendor__icontains')

    vendor = Orders.objects.filter(**filter).values('vendor').distinct()

    sub_category = Orders.objects.values('sub_category').distinct()
    category = Orders.objects.values('category').distinct()
    customer_status = Orders.objects.values('delayed_status_customer').distinct()
    vendor_status = Orders.objects.values('delayed_status_vendor').distinct()
    qc_status = Orders.objects.values('qc_status').distinct()
    qc_reason = Orders.objects.values('reason').distinct()
    country = Orders.objects.values('shipping_country').distinct()
    context['country'] = sorted(set([i['shipping_country'].lower().strip() for i in country if i['shipping_country']]))
    context['qc_reason'] = sorted(set([i['reason'].lower().strip() for i in qc_reason if i['reason']]))
    context['qc_status'] = sorted(set([i['qc_status'].lower().strip() for i in qc_status if i['qc_status']]))
    context['customer_status'] = sorted(set(
        [i['delayed_status_customer'].lower().strip() for i in customer_status if i['delayed_status_customer']]))
    context['vendor_status'] = sorted(set(
        [i['delayed_status_vendor'].lower().strip() for i in vendor_status if i['delayed_status_vendor']]))
    context['vrms'] = sorted(set([i['vrm'].lower().strip() for i in vrm if i['vrm']]))
    context['sub_category'] = sorted(
        set([i['sub_category'].lower().strip() for i in sub_category if i['sub_category']]))
    context['category'] = sorted(set([i['category'].lower().strip() for i in category if i['category']]))
    context['vendors'] = sorted(set([i['vendor'].lower().strip() for i in vendor if i['vendor']]))
    context['vrms'] = sorted(set([i['vrm'].lower().strip() for i in vrm if i['vrm']]))

    return context

#
# def get_vrm_vendors(vrm):
#     vendors = Orders.objects.filter(vrm__icontains=vrm).values('vendor').distinct()
#     return sorted(set([i['vendor'].lower().strip() for i in vendors if i['vendor']]))

def get_crm_dashboard_queryset(request):
    filter = dict()
    filter['is_active'] = True

    queryset = Orders.objects.filter(~Q(sku_type__in=['consignment', 'sd_brands']))

    if request.GET.get('q'):
        q = request.GET.get('q').strip()
        queryset = queryset.filter(Q(increment_id__istartswith=q) |
                                   Q(sku__istartswith=q) |
                                   Q(product_name__istartswith=q) |
                                   Q(customer_name__istartswith=q) |
                                   Q(customer_email__istartswith=q) |
                                   Q(customer_contact__istartswith=q) |
                                   Q(po_no__istartswith=q) |
                                   Q(grn_number__istartswith=q) |
                                   Q(rtv_no__istartswith=q) |
                                   Q(vendor_code__istartswith=q))

    if request.GET.get('vrm'):
        filter['vrm__icontains'] = request.GET['vrm']

    if request.GET.get('qc_status'):
        filter['qc_status__icontains'] = request.GET['qc_status']

    if request.GET.get('qc_reason'):
        filter['reason__icontains'] = request.GET['qc_reason']

    if request.GET.get('country'):
        filter['shipping_country__icontains'] = request.GET['country']

    if request.GET.get('vendor'):
        filter['vendor__icontains'] = request.GET['vendor']

    if request.GET.get('category'):
        filter['category__icontains'] = request.GET['category']

    if request.GET.get('sub_category'):
        filter['sub_category__icontains'] = request.GET['sub_category']

    if request.GET.get('vendor_status'):
        filter['delayed_status_vendor__icontains'] = request.GET['vendor_status']

    if request.GET.get('customer_status'):
        filter['delayed_status_customer__icontains'] = request.GET['customer_status']

    if request.GET.get('order_time'):
        filter['order_time__range'] = get_date_range(request.GET['order_time'])

    if request.GET.get('shipping_edd'):
        filter['shipping_edd__range'] = get_date_range(request.GET['shipping_edd'])

    if request.GET.get('customer_edd'):
        filter['customer_edd__range'] = get_date_range(request.GET['customer_edd'])

    if request.GET.get('vendor_edd'):
        filter['vendor_edd__range'] = get_date_range(request.GET['vendor_edd'])

    if request.GET.get('revised_vendor_edd'):
        filter['revised_vendor_edd__range'] = get_date_range(request.GET['revised_vendor_edd'])

    if request.GET.get('revised_customer_edd'):
        filter['revised_customer_edd__range'] = get_date_range(request.GET['revised_customer_edd'])

    if request.GET.get('edd_as_per_vendor'):
        filter['edd_as_per_vendor__range'] = get_date_range(request.GET['edd_as_per_vendor'])

    if request.GET.get('po_creation_time'):
        filter['po_creation_time__range'] = get_date_range(request.GET['po_creation_time'])

    if request.GET.get('po_acceptance_time'):
        filter['po_acceptance_time__range'] = get_date_range(request.GET['po_acceptance_time'])

    if request.GET.get('inward_time'):
        filter['inward_time__range'] = get_date_range(request.GET['inward_time'])

    if request.GET.get('qc_time'):
        filter['qc_time__range'] = get_date_range(request.GET['qc_time'])

    if request.GET.get('rtv_time'):
        filter['rtv_time__range'] = get_date_range(request.GET['rtv_time'])

    if request.GET.get('shipping_time'):
        filter['shipping_time__range'] = get_date_range(request.GET['shipping_time'])

    if request.GET.get('_50_per_vendor_edd'):
        filter['_50_per_vendor_edd__range'] = get_date_range(request.GET['_50_per_vendor_edd'])

    if request.GET.get('_2_days_vendor_edd'):
        filter['_2_days_vendor_edd__range'] = get_date_range(request.GET['_2_days_vendor_edd'])

    if request.GET.get('_50_per_customer_edd'):
        filter['_50_per_customer_edd__range'] = get_date_range(request.GET['_50_per_customer_edd'])

    if request.GET.get('_2_days_customer_edd'):
        filter['_2_days_customer_edd__range'] = get_date_range(request.GET['_2_days_customer_edd'])

    if request.GET.get('scheduled_pickup_date'):
        filter['order_editable__scheduled_pickup_date__range'] = get_date_range(request.GET['scheduled_pickup_date'])

    if request.GET.get('inward_time__isnull'):
        if request.GET.get('inward_time__isnull') == 'true':
            filter['inward_time__isnull'] = True
        else:
            filter['inward_time__isnull'] = False

    queryset = queryset.filter(**filter).select_related('order_editable')

    return queryset, filter


def queryset_to_csv_list(queryset):
    exclude = ['id', 'created_on', 'updated_on', 'is_active']
    extra = ['_50_per_calling_remark_', '_50_per_calling_edd_', '_2_days_calling_remark_', '_2_days_calling_edd_',
             '_overall_status_', '_edd_', '_50_per_mail_sent_', '_2_days_mail_sent_', '_scheduled_pickup_',
             '_scheduled_pickup_date_', '_remarks_']
    fields = Orders._meta.fields
    header = [i.name for i in fields if i.name not in exclude] + extra
    rows = [header]
    for instance in queryset:
        rows.append([newgetattr(instance, i) for i in header])
    return rows


def filter_ticket(created_at):
    created_date = datetime.strptime(created_at, '%Y-%m-%dT%H:%M:%SZ')
    filter_date = datetime.strptime('2015-01-01', '%Y-%m-%d')
    if created_date > filter_date:
        return True
    else:
        return False


class AWRMAEntity(object):
    def __init__(self, id, order_id, order_items, *args):
        self.id = id
        self.order_id = order_id
        self.order_items = list(set([extract_numbers(i) for i in order_items.split(":") if ';s' in i]))
        if not self.order_items:
            self.order_items = None
