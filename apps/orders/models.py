from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

from ..custom.utils import newdict
from ..custom.middleware.global_request import get_current_request
from ..base.models import BaseModel
from .constants import _50_per_calling_remark_choice, overall_status_choice, _2_days_calling_remark_choice, \
    mail_sent_choice


class Orders(BaseModel):
    order_id = models.IntegerField(verbose_name='Order Id')
    increment_id = models.CharField(verbose_name='Increment Id', max_length=50)
    order_time = models.DateTimeField(verbose_name='Order Time')
    sku = models.CharField(verbose_name='SKU', max_length=50)
    sku_type = models.CharField(max_length=50)
    product_name = models.CharField(max_length=300, null=True, blank=True)
    category = models.CharField(max_length=50, null=True, blank=True)
    sub_category = models.CharField(max_length=50, null=True, blank=True)
    vrm = models.CharField(max_length=100, null=True, blank=True)
    vendor = models.CharField(max_length=100, null=True, blank=True)
    vendor_code = models.CharField(max_length=100, null=True, blank=True)
    gsv = models.IntegerField()
    customer_name = models.CharField(max_length=200)
    customer_email = models.EmailField()
    customer_contact = models.CharField(max_length=50, null=True, blank=True)
    customer_edd = models.DateTimeField(null=True, blank=True)
    shipping_edd = models.DateTimeField(null=True, blank=True)
    vendor_edd = models.DateTimeField(null=True, blank=True)
    revised_customer_edd = models.DateTimeField(null=True, blank=True, verbose_name='Revised Cust EDD')
    revised_vendor_edd = models.DateTimeField(null=True, blank=True, verbose_name='Revised Vendor EDD')
    edd_as_per_vendor = models.DateTimeField(null=True, blank=True)
    delayed_status_customer = models.CharField(max_length=50, null=True, blank=True, verbose_name='Status Customer')
    delayed_status_vendor = models.CharField(max_length=50, null=True, blank=True, verbose_name='Status Vendor')
    vendor_sku = models.CharField(max_length=200, null=True, blank=True)
    qty_ordered = models.FloatField(default=0)
    qty_refunded = models.FloatField(default=0)
    qty_shipped = models.FloatField(default=0)
    customer_sla = models.IntegerField(default=0, null=True, blank=True)
    vendor_sla = models.IntegerField(default=0, null=True, blank=True)
    po_no = models.CharField(max_length=255, null=True, blank=True)
    po_creation_time = models.DateTimeField(null=True, blank=True)
    po_acceptance_time = models.DateTimeField(null=True, blank=True)
    po_status = models.CharField(max_length=20, null=True, blank=True)
    inward_time = models.DateTimeField(null=True, blank=True)
    grn_number = models.CharField(max_length=50, null=True, blank=True)
    qc_status = models.CharField(max_length=20, null=True, blank=True)
    reason = models.CharField(max_length=255, null=True, blank=True)
    qc_comment = models.CharField(null=True, blank=True, max_length=255)
    qc_time = models.DateTimeField(null=True, blank=True)
    rtv_no = models.CharField(null=True, blank=True, max_length=50)
    rtv_time = models.DateTimeField(null=True, blank=True)
    awb = models.CharField(max_length=50, blank=True, null=True)
    shipping_time = models.DateTimeField(null=True, blank=True)
    shipping_country = models.CharField(max_length=50, null=True, blank=True, verbose_name='Country')
    shipping_state = models.CharField(max_length=50, null=True, blank=True)
    shipping_city = models.CharField(max_length=50, null=True, blank=True)
    courier_partner = models.CharField(max_length=200, null=True, blank=True)
    _50_per_vendor_edd = models.DateTimeField(null=True, blank=True)
    _2_days_vendor_edd = models.DateTimeField(null=True, blank=True)
    _50_per_customer_edd = models.DateTimeField(null=True, blank=True, verbose_name='50% Cust EDD')
    _2_days_customer_edd = models.DateTimeField(null=True, blank=True, verbose_name='2 Days Cust EDD')
    num_days_delay = models.CharField(max_length=10, null=True, blank=True)
    pickup_request_time = models.DateTimeField(null=True, blank=True)

    class Meta:
        unique_together = ('increment_id', 'sku')

    def __unicode__(self):
        return 'Increment Id: {}, SKU: {}'.format(self.increment_id, self.sku)

    @property
    def _50_per_calling_remark_(self):
        key = self.order_editable._50_per_calling_remark
        if key:
            try:
                return dict(_50_per_calling_remark_choice)[key]
            except:
                return key
        return ''

    @property
    def _2_days_calling_remark_(self):
        key = self.order_editable._2_days_calling_remark
        if key:
            try:
                return dict(_2_days_calling_remark_choice)[key]
            except:
                return key
        return ''

    @property
    def _50_per_calling_edd_(self):
        return self.order_editable._50_per_calling_edd

    @property
    def _2_days_calling_edd_(self):
        return self.order_editable._2_days_calling_edd

    @property
    def _overall_status_(self):
        key = self.order_editable.overall_status
        if key:
            try:
                return dict(overall_status_choice)[key]
            except:
                return key
        return ''

    @property
    def _edd_(self):
        return self.order_editable.edd

    @property
    def _50_per_mail_sent_(self):
        if self.order_editable._50_per_mail_sent == 'N':
            return 'NO'
        else:
            return 'YES'

    @property
    def _2_days_mail_sent_(self):
        if self.order_editable._2_days_mail_sent == 'N':
            return 'NO'
        else:
            return 'YES'

    @property
    def _scheduled_pickup_(self):
        if self.order_editable.scheduled_pickup == 'N':
            return 'No'
        else:
            return 'Yes'

    @property
    def _scheduled_pickup_date_(self):
        return self.order_editable.scheduled_pickup_date

    @property
    def _pickup_request_time_(self):
        if self.pickup_request_time is None:
            return ''
        return self.pickup_request_time

    @property
    def _remarks_(self):
        if self.order_editable.remarks is None:
            return ''
        return self.order_editable.remarks


class OrderEditable(BaseModel):
    order = models.OneToOneField('orders.Orders', related_name='order_editable')
    _50_per_calling_remark = models.CharField(choices=_50_per_calling_remark_choice, null=True, blank=True,
                                              max_length=200)
    _50_per_calling_edd = models.DateTimeField(null=True, blank=True)
    _2_days_calling_remark = models.CharField(choices=_2_days_calling_remark_choice, null=True, blank=True,
                                              max_length=200)
    _2_days_calling_edd = models.DateTimeField(null=True, blank=True)
    overall_status = models.CharField(choices=overall_status_choice, null=True, blank=True, max_length=50)
    edd = models.DateTimeField(null=True, blank=True)
    _50_per_mail_sent = models.CharField(choices=mail_sent_choice, default='N', max_length=50)
    _2_days_mail_sent = models.CharField(choices=mail_sent_choice, default='N', max_length=50)
    scheduled_pickup = models.CharField(choices=mail_sent_choice, default='N', max_length=50)
    scheduled_pickup_date = models.DateTimeField(null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    updated_by = models.ForeignKey(User, null=True, blank=True)

    def save(self, *args, **kwargs):
        request = get_current_request()
        if request:
            self.updated_by = request.user
        if self.pk is None:
            created = True
        else:
            created = False
        super(OrderEditable, self).save(*args, **kwargs)
        self.create_history(created, request)

    def create_history(self, created, request):
        data = dict()
        data['order'] = self
        data['_50_per_calling_remark'] = self._50_per_calling_remark
        data['_50_per_calling_edd'] = self._50_per_calling_edd
        data['_2_days_calling_remark'] = self._2_days_calling_remark
        data['_2_days_calling_edd'] = self._2_days_calling_edd
        data['overall_status'] = self.overall_status
        data['edd'] = self.edd
        data['_50_per_mail_sent'] = self._50_per_mail_sent
        data['_2_days_mail_sent'] = self._2_days_mail_sent
        data['scheduled_pickup'] = self.scheduled_pickup
        data['scheduled_pickup_date'] = self.scheduled_pickup_date
        data['remarks'] = self.remarks
        data['updated_by'] = self.updated_by
        OrderEditHistory.objects.create(**data)

        if not created:
            from .tasks import update_analytics_db

            data.pop('order')
            data['sku'] = self.order.sku
            data['increment_id'] = self.order.increment_id
            if data['updated_by']:
                data['updated_by'] = data['updated_by'].get_full_name()
            bind = newdict()
            bind.update(data)
            update_analytics_db.delay(bind)


class OrderEditHistory(BaseModel):
    order = models.ForeignKey('orders.OrderEditable', related_name='order_edit_history')
    _50_per_calling_remark = models.CharField(null=True, blank=True, max_length=200)
    _50_per_calling_edd = models.DateTimeField(null=True, blank=True)
    _2_days_calling_remark = models.CharField(null=True, blank=True, max_length=200)
    _2_days_calling_edd = models.DateTimeField(null=True, blank=True)
    overall_status = models.CharField(null=True, blank=True, max_length=50)
    edd = models.DateTimeField(null=True, blank=True)
    _50_per_mail_sent = models.CharField(default='N', max_length=50)
    _2_days_mail_sent = models.CharField(default='N', max_length=50)
    scheduled_pickup = models.CharField(default='N', max_length=50)
    scheduled_pickup_date = models.DateTimeField(null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)
    updated_by = models.ForeignKey(User, null=True, blank=True)

    def __unicode__(self):
        return '{}'.format(self.order.order)


class CRM(object):
    def __init__(self, order_time, month, increment_id, order_id, sku, vendor_sku, sku_type, vendor, vendor_code,
                 product_name,
                 category, sub_category, gsv, customer_first_name, customer_last_name, customer_email, telephone,
                 qty_ordered, qty_refunded, qty_shipped, customer_sla, customer_edd, shipping_edd, vendor_sla,
                 vendor_edd, po_no, po_cost, po_creation_time, po_status, po_acceptance_time, edd_as_per_vendor,
                 pickup_request_time, inward_time,
                 grn_no, qc_status, reason, qc_comment, qc_time, rtv_no, rtv_time, shipping_time, awb_no, courier_code,
                 courier_partner, shipping_country, state, city, pincode, revised_vendor_edd, vrm_name,
                 revised_customer_edd,
                 delayed_status_customer, delayed_status_vendor, num_days_delay, per_50_vendor_edd, days_2_vendor_edd,
                 _50_per_customer_edd, _2_days_customer_edd):
        self.vendor_code = vendor_code
        self.num_days_delay = num_days_delay
        self.order_time = order_time
        self.pickup_request_time = pickup_request_time
        self.increment_id = increment_id
        self.order_id = order_id
        self.sku = sku
        self.vendor_sku = vendor_sku
        self.sku_type = sku_type
        self.vendor = vendor
        self.product_name = product_name
        self.category = category
        self.sub_category = sub_category
        self.gsv = gsv
        self.customer_name = '{} {}'.format(customer_first_name, customer_last_name).strip()
        self.customer_email = customer_email
        self.customer_contact = telephone
        self.qty_ordered = qty_ordered
        self.qty_refunded = qty_refunded
        self.qty_shipped = qty_shipped
        self.customer_sla = customer_sla
        self.customer_edd = customer_edd
        self.shipping_edd = shipping_edd
        self.vendor_sla = vendor_sla
        self.vendor_edd = vendor_edd
        self.po_no = po_no
        self.po_creation_time = po_creation_time
        self.po_status = po_status
        self.po_acceptance_time = po_acceptance_time
        self.edd_as_per_vendor = edd_as_per_vendor
        self.inward_time = inward_time
        self.grn_number = grn_no
        self.qc_status = qc_status
        self.reason = reason
        self.qc_comment = qc_comment
        self.qc_time = qc_time
        self.rtv_no = rtv_no
        self.rtv_time = rtv_time
        self.shipping_time = shipping_time
        self.awb = awb_no
        self.courier_partner = courier_partner
        self.shipping_country = shipping_country
        self.shipping_state = state
        self.shipping_city = city
        self._50_per_vendor_edd = per_50_vendor_edd
        self._2_days_vendor_edd = days_2_vendor_edd
        self.vrm = vrm_name
        self.delayed_status_vendor = delayed_status_vendor
        self.revised_customer_edd = revised_customer_edd
        self.delayed_status_customer = delayed_status_customer
        self._50_per_customer_edd = _50_per_customer_edd
        self._2_days_customer_edd = _2_days_customer_edd
        self.revised_vendor_edd = revised_vendor_edd

        # def __setattr__(self, key, value):
        #     if type(value) is datetime:
        #         self.__dict__[key] = value + timedelta(hours=5, minutes=30)
        #     return super(CRM, self).__setattr__(key, value)
