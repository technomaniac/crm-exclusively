from django.conf.urls import patterns, url

from .views import *

urlpatterns = patterns('apps.orders.views',
                       url(r'^dashboard/$', DashboardView.as_view(), name='dashboard'),
                       url(r'^get_editable_options/$', get_editable_options, name='get_editable_options'),
                       #url(r'^vrm_vendors/$', vrm_vendors, name='vrm_vendors'),
                       )
