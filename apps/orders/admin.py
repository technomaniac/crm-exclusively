from django.contrib import admin

from .models import *


# Register your models here.

class OrderAdmin(admin.ModelAdmin):
    list_display = ('order_time', 'increment_id', 'sku', 'sku_type')
    search_fields = ('increment_id', 'sku')


class OrderEditableAdmin(admin.ModelAdmin):
    list_display = ('order', '_50_per_calling_remark', '_50_per_calling_edd',
                    '_2_days_calling_remark', '_2_days_calling_edd', 'overall_status', 'edd',
                    '_50_per_mail_sent', '_2_days_mail_sent', 'updated_by', 'updated_on', 'created_on')

    search_fields = ('order__increment_id', 'order__sku')


admin.site.register(Orders, OrderAdmin)
admin.site.register(OrderEditable, OrderEditableAdmin)
admin.site.register(OrderEditHistory)
