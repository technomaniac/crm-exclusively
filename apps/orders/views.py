# Create your views here.
import csv
from datetime import datetime, timedelta

from django.http.response import StreamingHttpResponse

from django.shortcuts import render

from django.views.generic.base import View

from django_tables2.config import RequestConfig

from ..custom.utils import get_json_response, Echo
from ..custom.mixins import GeneralLoginRequiredMixin
from .utils import get_dashboard_context, get_crm_dashboard_queryset, queryset_to_csv_list
from .tables import OrderTable
from .models import OrderEditable
from .constants import _2_days_calling_remark_choice, _50_per_calling_remark_choice, overall_status_choice, DATE_FORMAT, \
    mail_sent_choice


class DashboardView(GeneralLoginRequiredMixin, View):
    template_name = 'orders/dashboard.html'

    def post(self, request):
        if request.is_ajax():
            pk = request.POST['id']
            order = OrderEditable.objects.get(order__pk=pk)
            if request.POST.get('_50_per_calling_remark'):
                order._50_per_calling_remark = request.POST['_50_per_calling_remark'].encode('utf-8')

            if request.POST.get('_50_per_calling_edd'):
                order._50_per_calling_edd = datetime.strptime(request.POST.get('_50_per_calling_edd'),
                                                              DATE_FORMAT) + timedelta(hours=5, minutes=30)

            if request.POST.get('_2_days_calling_remark'):
                order._2_days_calling_remark = request.POST.get('_2_days_calling_remark').encode('utf-8')

            if request.POST.get('_2_days_calling_edd'):
                order._2_days_calling_edd = datetime.strptime(request.POST.get('_2_days_calling_edd'),
                                                              DATE_FORMAT) + timedelta(hours=5, minutes=30)

            if request.POST.get('overall_status'):
                order.overall_status = request.POST.get('overall_status').encode('utf-8')

            if request.POST.get('_50_per_mail_sent'):
                order._50_per_mail_sent = request.POST.get('_50_per_mail_sent').encode('utf-8')

            if request.POST.get('_2_days_mail_sent'):
                order._2_days_mail_sent = request.POST.get('_2_days_mail_sent').encode('utf-8')

            if request.POST.get('edd'):
                order.edd = datetime.strptime(request.POST.get('edd'), DATE_FORMAT) + timedelta(hours=5, minutes=30)

            if 'remarks' in request.POST:
                order.remarks = request.POST['remarks'].encode('utf-8').strip()

            if request.POST.get('scheduled_pickup'):
                order.scheduled_pickup = request.POST.get('scheduled_pickup')
                if request.POST.get('scheduled_pickup_date') and request.POST.get('scheduled_pickup') == 'Y':
                    order.scheduled_pickup_date = datetime.strptime(request.POST.get('scheduled_pickup_date'),
                                                                DATE_FORMAT) + timedelta(hours=5, minutes=30)

            order.save()

            return get_json_response({'order_id': order.pk})
            # except Exception as e:
            #     return get_json_response({'error': e.message})

    def get(self, request):

        queryset, filter = get_crm_dashboard_queryset(request)

        context = get_dashboard_context(filter)

        if 'download' in request.GET:
            return self.download_csv(request, queryset)

        table = OrderTable(queryset, per_page=request.GET.get('per_page', 15), page=request.GET.get('page', 1),
                           role=request.user.profile.role)
        RequestConfig(request).configure(table)
        context['table'] = table
        return render(request, self.template_name, context)

    @staticmethod
    def download_csv(request, queryset):
        """A view that streams a large CSV file."""
        # Generate a sequence of rows. The range is based on the maximum number of
        # rows that can be handled by a single sheet in most spreadsheet
        # applications.
        filename = '{}_ORDERS_{}.csv'.format(request.user.profile.role.upper(),
                                             datetime.now().strftime('%Y-%m-%d_%H-%M'))
        rows = queryset_to_csv_list(queryset)
        pseudo_buffer = Echo()
        writer = csv.writer(pseudo_buffer)
        response = StreamingHttpResponse((writer.writerow(row) for row in rows),
                                         content_type="text/csv")
        response['Content-Disposition'] = 'attachment; filename=' + filename
        return response


def get_editable_options(request):
    context = dict()
    if request.user.profile.role == 'CX':
        context['_50_per_mail_sent'] = dict(mail_sent_choice)
        context['_2_days_mail_sent'] = dict(mail_sent_choice)
    elif request.user.profile.role == 'SRM':
        context['overall_status'] = dict(overall_status_choice)
        context['_2_days_calling_remark'] = dict(_2_days_calling_remark_choice)
        context['_50_per_calling_remark'] = dict(_50_per_calling_remark_choice)
        context['scheduled_pickup'] = dict(mail_sent_choice)
    else:
        context['_50_per_mail_sent'] = dict(mail_sent_choice)
        context['_2_days_mail_sent'] = dict(mail_sent_choice)
        context['overall_status'] = dict(overall_status_choice)
        context['_2_days_calling_remark'] = dict(_2_days_calling_remark_choice)
        context['_50_per_calling_remark'] = dict(_50_per_calling_remark_choice)
        context['scheduled_pickup'] = dict(mail_sent_choice)
    return get_json_response(context)

# def vrm_vendors(request):
#     return get_json_response(get_vrm_vendors)
