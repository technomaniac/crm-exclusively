from datetime import datetime
import time

from django.db.models.query import QuerySet
from django.db import models


class BaseQuery(QuerySet):
    def update(self, *args, **kwargs):
        for i in self.all():
            i.modified_on = datetime.now()
        super(BaseQuery, self).update(**kwargs)

    def active(self):
        return self.filter(is_active=True)


class BaseManager(models.Manager):
    def get_queryset(self):
        return BaseQuery(self.model).order_by('-created_on')


class BaseModel(models.Model):
    created_on = models.IntegerField(null=True, blank=True, default=int(time.time()))
    updated_on = models.IntegerField(null=True, blank=True, default=int(time.time()))
    is_active = models.BooleanField(default=True)

    objects = BaseManager()

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        if not self.pk:
            self.created_on = int(time.time())
        self.updated_on = int(time.time())
        super(BaseModel, self).save(*args, **kwargs)

    @property
    def created(self):
        return datetime.fromtimestamp(self.created_on)

    @property
    def updated(self):
        return datetime.fromtimestamp(self.updated_on)
