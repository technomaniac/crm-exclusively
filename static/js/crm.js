﻿/**
 * jQuery.query - Query String Modification and Creation for jQuery
 * Written by Blair Mitchelmore (blair DOT mitchelmore AT gmail DOT com)
 * Licensed under the WTFPL (http://sam.zoy.org/wtfpl/).
 * Date: 2009/8/13
 *
 * @author Blair Mitchelmore
 * @version 2.2.2
 *
 **/

new function (settings) {
    // Various Settings
    var $separator = settings.separator || '&';
    var $spaces = settings.spaces === false ? false : true;
    var $suffix = settings.suffix === false ? '' : '[]';
    var $prefix = settings.prefix === false ? false : true;
    var $hash = $prefix ? settings.hash === true ? "#" : "?" : "";
    var $numbers = settings.numbers === false ? false : true;

    jQuery.query = new function () {
        var is = function (o, t) {
            return o != undefined && o !== null && (!!t ? o.constructor == t : true);
        };
        var parse = function (path) {
            var m, rx = /\[([^[]*)\]/g, match = /^([^[]+)(\[.*\])?$/.exec(path), base = match[1], tokens = [];
            while (m = rx.exec(match[2])) tokens.push(m[1]);
            return [base, tokens];
        };
        var set = function (target, tokens, value) {
            var o, token = tokens.shift();
            if (typeof target != 'object') target = null;
            if (token === "") {
                if (!target) target = [];
                if (is(target, Array)) {
                    target.push(tokens.length == 0 ? value : set(null, tokens.slice(0), value));
                } else if (is(target, Object)) {
                    var i = 0;
                    while (target[i++] != null);
                    target[--i] = tokens.length == 0 ? value : set(target[i], tokens.slice(0), value);
                } else {
                    target = [];
                    target.push(tokens.length == 0 ? value : set(null, tokens.slice(0), value));
                }
            } else if (token && token.match(/^\s*[0-9]+\s*$/)) {
                var index = parseInt(token, 10);
                if (!target) target = [];
                target[index] = tokens.length == 0 ? value : set(target[index], tokens.slice(0), value);
            } else if (token) {
                var index = token.replace(/^\s*|\s*$/g, "");
                if (!target) target = {};
                if (is(target, Array)) {
                    var temp = {};
                    for (var i = 0; i < target.length; ++i) {
                        temp[i] = target[i];
                    }
                    target = temp;
                }
                target[index] = tokens.length == 0 ? value : set(target[index], tokens.slice(0), value);
            } else {
                return value;
            }
            return target;
        };

        var queryObject = function (a) {
            var self = this;
            self.keys = {};

            if (a.queryObject) {
                jQuery.each(a.get(), function (key, val) {
                    self.SET(key, val);
                });
            } else {
                self.parseNew.apply(self, arguments);
            }
            return self;
        };

        queryObject.prototype = {
            queryObject: true,
            parseNew: function () {
                var self = this;
                self.keys = {};
                jQuery.each(arguments, function () {
                    var q = "" + this;
                    q = q.replace(/^[?#]/, ''); // remove any leading ? || #
                    q = q.replace(/[;&]$/, ''); // remove any trailing & || ;
                    if ($spaces) q = q.replace(/[+]/g, ' '); // replace +'s with spaces

                    jQuery.each(q.split(/[&;]/), function () {
                        var key = decodeURIComponent(this.split('=')[0] || "");
                        var val = decodeURIComponent(this.split('=')[1] || "");

                        if (!key) return;

                        if ($numbers) {
                            if (/^[+-]?[0-9]+\.[0-9]*$/.test(val)) // simple float regex
                                val = parseFloat(val);
                            else if (/^[+-]?[1-9][0-9]*$/.test(val)) // simple int regex
                                val = parseInt(val, 10);
                        }

                        val = (!val && val !== 0) ? true : val;

                        self.SET(key, val);
                    });
                });
                return self;
            },
            has: function (key, type) {
                var value = this.get(key);
                return is(value, type);
            },
            GET: function (key) {
                if (!is(key)) return this.keys;
                var parsed = parse(key), base = parsed[0], tokens = parsed[1];
                var target = this.keys[base];
                while (target != null && tokens.length != 0) {
                    target = target[tokens.shift()];
                }
                return typeof target == 'number' ? target : target || "";
            },
            get: function (key) {
                var target = this.GET(key);
                if (is(target, Object))
                    return jQuery.extend(true, {}, target);
                else if (is(target, Array))
                    return target.slice(0);
                return target;
            },
            SET: function (key, val) {
                var value = !is(val) ? null : val;
                var parsed = parse(key), base = parsed[0], tokens = parsed[1];
                var target = this.keys[base];
                this.keys[base] = set(target, tokens.slice(0), value);
                return this;
            },
            set: function (key, val) {
                return this.copy().SET(key, val);
            },
            REMOVE: function (key, val) {
                if (val) {
                    var target = this.GET(key);
                    if (is(target, Array)) {
                        for (tval in target) {
                            target[tval] = target[tval].toString();
                        }
                        var index = $.inArray(val, target);
                        if (index >= 0) {
                            key = target.splice(index, 1);
                            key = key[index];
                        } else {
                            return;
                        }
                    } else if (val != target) {
                        return;
                    }
                }
                return this.SET(key, null).COMPACT();
            },
            remove: function (key, val) {
                return this.copy().REMOVE(key, val);
            },
            EMPTY: function () {
                var self = this;
                jQuery.each(self.keys, function (key, value) {
                    delete self.keys[key];
                });
                return self;
            },
            load: function (url) {
                var hash = url.replace(/^.*?[#](.+?)(?:\?.+)?$/, "$1");
                var search = url.replace(/^.*?[?](.+?)(?:#.+)?$/, "$1");
                return new queryObject(url.length == search.length ? '' : search, url.length == hash.length ? '' : hash);
            },
            empty: function () {
                return this.copy().EMPTY();
            },
            copy: function () {
                return new queryObject(this);
            },
            COMPACT: function () {
                function build(orig) {
                    var obj = typeof orig == "object" ? is(orig, Array) ? [] : {} : orig;
                    if (typeof orig == 'object') {
                        function add(o, key, value) {
                            if (is(o, Array))
                                o.push(value);
                            else
                                o[key] = value;
                        }

                        jQuery.each(orig, function (key, value) {
                            if (!is(value)) return true;
                            add(obj, key, build(value));
                        });
                    }
                    return obj;
                }

                this.keys = build(this.keys);
                return this;
            },
            compact: function () {
                return this.copy().COMPACT();
            },
            toString: function () {
                var i = 0, queryString = [], chunks = [], self = this;
                var encode = function (str) {
                    str = str + "";
                    str = encodeURIComponent(str);
                    if ($spaces) str = str.replace(/%20/g, "+");
                    return str;
                };
                var addFields = function (arr, key, value) {
                    if (!is(value) || value === false) return;
                    var o = [encode(key)];
                    if (value !== true) {
                        o.push("=");
                        o.push(encode(value));
                    }
                    arr.push(o.join(""));
                };
                var build = function (obj, base) {
                    var newKey = function (key) {
                        return !base || base == "" ? [key].join("") : [base, "[", key, "]"].join("");
                    };
                    jQuery.each(obj, function (key, value) {
                        if (typeof value == 'object')
                            build(value, newKey(key));
                        else
                            addFields(chunks, newKey(key), value);
                    });
                };

                build(this.keys);

                if (chunks.length > 0) queryString.push($hash);
                queryString.push(chunks.join($separator));

                return queryString.join("");
            }
        };

        return new queryObject(location.search, location.hash);
    };
}(jQuery.query || {}); // Pass in jQuery.query as settings object

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }

        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$('#reset').click(function () {
    window.location.href = '/orders/dashboard/';
});

$('#search_crm').keypress(function (e) {
    if (e.which == 13) {
        window.location.search = jQuery.query.set('q', $(this).val());
    }
});

$('.selectable').change(function () {
    window.location.search = jQuery.query.set($(this).attr('id'), $(this).val());
});

$('#export-csv').click(function () {
    window.location.search = jQuery.query.set('download', true);
});

$('#filter').click(function () {
    var date_type = $('#date-filter').val();
    var start_date = $('#start-date').val();
    var end_date = $('#end-date').val();
    var url_string = date_type + '=' + start_date + '-' + end_date;
    if (date_type != '' && start_date != '' && end_date != '') {
        var url = location.href;
        var options = $('#date-filter option');
        options.each(function () {
            var regex = new RegExp('&?' + $(this).val() + '=([^&]$|[^&]*)', 'i');
            url = url.replace(regex, "");
        });

        if (url.indexOf('?') > 0) {
            url += '&' + url_string;
        } else {
            url += '?' + url_string;
        }
        if (url.indexOf('?&') > 0) {
            url = url.replace('?&', '?');
        }
        location.href = url;
    }
});

function urldecode(str) {
    return decodeURIComponent((str + '').replace(/\+/g, '%20'));
}

$(document).ready(function () {
    var params = getUrlVars();
    //console.log(params);
    $.each(params, function (e, key) {
        if (key != location.href) {
            var value = urldecode(params[key]);
            if (value != undefined) {
                $('#' + key + ' option').each(function () {
                    if ($(this).val() == value) {
                        $(this).attr('selected', 'selected');
                    }
                });
            }
        }
    });
    $('#start-date').datepicker({dateFormat: 'dd/mm/yy'});
    $('#end-date').datepicker({dateFormat: 'dd/mm/yy'});
});

$('#user-selector').change(function () {
    if ($(this).val() == 'reset_pass') {
        window.location = '/change_password/';
    } else if ($(this).val() == 'logout') {
        window.location = '/logout/';
    }
});

function update_order(data) {
    $.ajax({
        url: "/orders/dashboard/",
        type: "POST",
        data: data,
        success: function (html) {
            //$("#pager").html(html);
        }
    });
}


function get_editable_options(ele, disable_elements) {
    $.ajax({
        url: "/orders/get_editable_options/",
        type: "GET",
        contentType: "application/json",
        dataType: 'json',
        success: function (json) {
            $.each(json, function (idx, choice) {
                if ($.inArray(idx, disable_elements) == -1) {
                    var element = ele.parent().parent().find('.' + idx);
                    var select = '<select class="select-ediatble">';
                    select += '<option selected style="display:none"></option>';
                    $.each(choice, function (key, value) {
                        var selected = '';
                        if (element.text() == value) {
                            selected = ' selected';
                        }
                        select += '<option value="' + key + '"' + selected + '>' + value + '</option>';
                    });
                    select += '</select>';
                    element.html(select);
                }
            });
        }
    });
}

$('.action button').click(function () {
    var id = $(this).attr('id');
    var _50_per_calling_remark_element = $(this).parent().parent().find('._50_per_calling_remark');
    var _2_days_calling_remark_element = $(this).parent().parent().find('._2_days_calling_remark');
    var overall_status_element = $(this).parent().parent().find('.overall_status');
    var _50_per_calling_edd_element = $(this).parent().parent().find('._50_per_calling_edd');
    var _2_days_calling_edd_element = $(this).parent().parent().find('._2_days_calling_edd');
    var edd_element = $(this).parent().parent().find('.edd');
    var _50_per_mail_sent_element = $(this).parent().parent().find('._50_per_mail_sent');
    var _2_days_mail_sent_element = $(this).parent().parent().find('._2_days_mail_sent');
    var scheduled_pickup_element = $(this).parent().parent().find('.scheduled_pickup');
    var scheduled_pickup_date_element = $(this).parent().parent().find('.scheduled_pickup_date');
    var remarks_element = $(this).parent().parent().find('.remarks');

    var disable_selects = [];
    var disable_inputs = [];
    if (_50_per_calling_remark_element.text() != '' && _50_per_calling_edd_element.text() != '') {
        disable_selects.push('_50_per_calling_remark');
        disable_inputs.push(_50_per_calling_edd_element);
    }

    if (_2_days_calling_remark_element.text() != '' && _2_days_calling_edd_element.text() != '') {
        disable_selects.push('_2_days_calling_remark');
        disable_inputs.push(_2_days_calling_edd_element);
    }

    if ($(this).text() == 'Edit') {
        var role = $.trim($('#role-header').text());

        get_editable_options($(this), disable_selects);

        if (role == 'SRM Dashboard' || role == 'ADMIN Dashboard') {

            if ($.inArray(_50_per_calling_edd_element, disable_inputs) == -1) {
                _50_per_calling_edd_element.html('<input type="text" value="' + _50_per_calling_edd_element.text() + '">');
                _50_per_calling_edd_element.find('input').datepicker({minDate: 0, dateFormat: 'dd/mm/yy'});
            }

            if ($.inArray(_2_days_calling_edd_element, disable_inputs) == -1) {
                _2_days_calling_edd_element.html('<input type="text" value="' + _2_days_calling_edd_element.text() + '">');
                _2_days_calling_edd_element.find('input').datepicker({minDate: 0, dateFormat: 'dd/mm/yy'});
            }

            if ($.inArray(edd_element, disable_inputs) == -1) {
                edd_element.html('<input type="text" value="' + edd_element.text() + '">');
                edd_element.find('input').datepicker({minDate: 0, dateFormat: 'dd/mm/yy'});
            }
            scheduled_pickup_date_element.html('<input type="text" value="' + scheduled_pickup_date_element.text() + '">');
            scheduled_pickup_date_element.find('input').datepicker({minDate: 0, dateFormat: 'dd/mm/yy'});
            remarks_element.html('<input type="text" value="' + remarks_element.text() + '">');
        }

        $(this).html('Save');

    } else {

        data = {};
        data['id'] = id;

        if (_50_per_calling_remark_element.find('select').length) {
            data['_50_per_calling_remark'] = _50_per_calling_remark_element.find('select').val();
        }
        if (_50_per_calling_edd_element.find('input').length) {
            data['_50_per_calling_edd'] = _50_per_calling_edd_element.find('input').val();
        }

        if ((data['_50_per_calling_remark'] == 'IP-ADF' || data['_50_per_calling_remark'] == 'NIP-ADOF'
            || data['_50_per_calling_remark'] == 'PI-DID') && data['_50_per_calling_edd'] == '') {
            alert('Please select 50% Calling EDD');
            _50_per_calling_edd_element.find('input').focus();
            return false;
        }

        if (_2_days_calling_remark_element.find('select').length) {
            data['_2_days_calling_remark'] = _2_days_calling_remark_element.find('select').val();
        }

        if (_2_days_calling_edd_element.find('input').length) {
            data['_2_days_calling_edd'] = _2_days_calling_edd_element.find('input').val();
        }

        if (data['_2_days_calling_remark'] == 'DEL' && data['_2_days_calling_edd'] == '') {
            alert('Please select 2 Days Calling EDD');
            _2_days_calling_edd_element.find('input').focus();
            return false;
        }

        if (edd_element.find('input').length) {
            data['edd'] = edd_element.find('input').val();
        }
        if (overall_status_element.find('select').length) {
            data['overall_status'] = overall_status_element.find('select').val();
        }

        if (scheduled_pickup_element.find('select').length) {
            data['scheduled_pickup'] = scheduled_pickup_element.find('select').val();
        }
        if (scheduled_pickup_date_element.find('input').length) {
            data['scheduled_pickup_date'] = scheduled_pickup_date_element.find('input').val();
        }
        if (remarks_element.find('input').length) {
            data['remarks'] = remarks_element.find('input').val();
        }
        if (data['scheduled_pickup'] == 'Y' && data['scheduled_pickup_date'] == '') {
            alert('Please select Scheduled Pickup Date');
            scheduled_pickup_date_element.find('input').focus();
            return false;
        }
        if ((data['overall_status'] == 'EDDA' || data['overall_status'] == 'DD' || data['overall_status'] == 'PC' ||
            data['overall_status'] == 'QCRFU') && data['edd'] == '') {
            alert('Please select EDD');
            edd_element.find('input').focus();
            return false;
        }

        if (_50_per_calling_edd_element.find('input').length) {
            _50_per_calling_edd_element.html(_50_per_calling_edd_element.find('input').val());
        }
        if (_50_per_calling_remark_element.find('select').length) {
            _50_per_calling_remark_element.html(_50_per_calling_remark_element.find('select :selected').text());
        }
        if (overall_status_element.find('select').length) {
            overall_status_element.html(overall_status_element.find('select :selected').text());
        }
        if (edd_element.find('input').length) {
            edd_element.html(edd_element.find('input').val());
        }
        if (_2_days_calling_remark_element.find('select').length) {
            _2_days_calling_remark_element.html(_2_days_calling_remark_element.find('select :selected').text());
        }
        if (_2_days_calling_edd_element.find('input').length) {
            _2_days_calling_edd_element.html(_2_days_calling_edd_element.find('input').val());
        }
        if (_50_per_mail_sent_element.find('select').length) {
            data['_50_per_mail_sent'] = _50_per_mail_sent_element.find('select').val();
            data['_2_days_mail_sent'] = _2_days_mail_sent_element.find('select').val();

            _50_per_mail_sent_element.html(_50_per_mail_sent_element.find('select :selected').text());
            _2_days_mail_sent_element.html(_2_days_mail_sent_element.find('select :selected').text());
        }
        if (scheduled_pickup_element.find('select').length) {
            scheduled_pickup_element.html(scheduled_pickup_element.find('select :selected').text());
        }
        if (scheduled_pickup_date_element.find('input').length) {
            scheduled_pickup_date_element.html(scheduled_pickup_date_element.find('input').val());
        }

        if (remarks_element.find('input').length) {
            remarks_element.html(remarks_element.find('input').val());
        }
        update_order(data);
        $(this).html('Edit');

    }
});


var mywindow = $(window);
var mypos = mywindow.scrollTop();
var up = false;
var newscroll;

mywindow.scroll(function () {
    newscroll = mywindow.scrollTop();
    if (newscroll > mypos && !up) {
        $('.hideable').stop().slideToggle();
        up = !up;
        console.log(up);
    } else if (newscroll < mypos && up) {
        $('.hideable').stop().slideToggle();
        up = !up;
    }
    mypos = newscroll;
});